package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os/exec"
	"path"

	"gopkg.in/fsnotify.v1"
)

var SpeakerPipe io.WriteCloser

var DefaultLang string

const presencePath = "/tmp/isPresent"

func main() {
	var bind = flag.String("bind", ":7030", "Bind port/socket")
	flag.StringVar(&DefaultLang, "lang", "fr", "Default language to speak")
	flag.Parse()

	go func() {
		for {
			cmd := exec.Command("espeak", "-z", "-m", "-s", "190")
			if in, err := cmd.StdinPipe(); err != nil {
				log.Fatal("Unable to create pipe to espeak:", err)
			} else {
				SpeakerPipe = in
			}
			cmd.Run()
		}
	}()

	if watcher, err := fsnotify.NewWatcher(); err != nil {
		log.Fatal(err)
	} else {
		if err := watcher.Add(path.Dir(presencePath)); err != nil {
			log.Fatal("Unable to watch: ", path.Dir(presencePath), ": ", err)
		}

		go func() {
			defer watcher.Close()
			for {
				select {
				case ev := <-watcher.Events:
					if path.Base(ev.Name) == presencePath {
						if ev.Op & fsnotify.Create == fsnotify.Create {
							log.Println("Presence file created!")
						} else if ev.Op & fsnotify.Remove == fsnotify.Remove {
							log.Println("Presence file removed!")
						}
					}
				case err := <-watcher.Errors:
					log.Println("watcher error:", err)
				}
			}
		}()
	}

	// Register handlers
	http.Handle("/notify", http.StripPrefix("/notify", NotifyHandler{}))

	// Serve pages
	log.Println(fmt.Sprintf("Ready, listening on %s", *bind))
	if err := http.ListenAndServe(*bind, nil); err != nil {
		log.Fatal("Unable to listen and serve: ", err)
	}
}
